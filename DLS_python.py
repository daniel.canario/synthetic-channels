import TimeTagger as TT
import numpy as np
import matplotlib.pyplot as plt

#------------------------------------------------------------------------------
#============================= Time Tagger Setup ==============================
#------------------------------------------------------------------------------

virtual_tagger = TT.createTimeTaggerVirtual()
virtual_tagger.setReplaySpeed(1)

#------------------------------------------------------------------------------
#============================= Numeric Constants ==============================
#------------------------------------------------------------------------------

rng = np.random.default_rng(seed=1,)
OUT_CHANNEL=1
DECAY_TIME=1E8 # 1 microsecond in picoseconds
T_period=1E6
std_dev=np.sqrt(2*T_period/DECAY_TIME) # from theory of Brownian motion
N=16
COUNTRATE=1E6 # in Hz

#------------------------------------------------------------------------------
#================================= DLS Class ==================================
#------------------------------------------------------------------------------

class DLS(TT.Experimental.PhotonGenerator):
    def __init__(self, tagger, countrate, channel):
        super().__init__(tagger, countrate, channel)
        self.finalize_init()
        self.phases = rng.uniform(0, 2*np.pi, N)

    def __del__(self):
        self.stop()

    def get_intensity(self):
        self.phases += rng.normal(0, std_dev, N)
        return 1/ N * np.abs(np.sum(np.exp(self.phases*1j)))**2

dls_signal_py = DLS(tagger=virtual_tagger,
                 countrate=COUNTRATE,
                 channel=OUT_CHANNEL)

dls_signal_cpp=TT.Experimental.DlsSignalGenerator(
                    tagger=virtual_tagger,
                    decay_time=DECAY_TIME,
                    countrate=COUNTRATE,
                    output_channel=OUT_CHANNEL + 1)

#------------------------------------------------------------------------------
#============================ Measurement Objects =============================
#------------------------------------------------------------------------------

E_START=-7
E_END=-3
OUT_CHANNEL = 1
BIN_WIDTH = 1e8
N_BINS = int(1e3)

log_histo = TT.HistogramLogBins(
    tagger=virtual_tagger,
    click_channel=OUT_CHANNEL,
    start_channel=OUT_CHANNEL,
    exp_start=E_START,
    exp_stop=E_END,
    n_bins=100,
)

log_histo2 = TT.HistogramLogBins(
    tagger=virtual_tagger,
    click_channel=OUT_CHANNEL + 1,
    start_channel=OUT_CHANNEL +1,
    exp_start=E_START,
    exp_stop=E_END,
    n_bins=100,
)

counter = TT.Counter(
    tagger=virtual_tagger,
    channels=[OUT_CHANNEL],
    binwidth=BIN_WIDTH,
    n_values=N_BINS,
)

count_rate=TT.Countrate(
    tagger=virtual_tagger,
    channels=[OUT_CHANNEL])

#------------------------------------------------------------------------------
#=========================== Plot Synthetic Signal ============================
#------------------------------------------------------------------------------

PHI = 0.5 * (1 + np.sqrt(5))  # golden ratio

fig, axes = plt.subplot_mosaic(
    [["top"],
     ["bottom"]], width_ratios=[1 / PHI]
)
fig.subplots_adjust(hspace=.5)
fig.canvas.manager.set_window_title('DLS Synthetic Channel')

x_range=np.logspace(E_START, E_END, num=100) # in seconds
y_range=np.exp(-x_range/(DECAY_TIME)*1e12)**2*(1-1/N)+1

while plt.fignum_exists(1):
    plt.pause(.25)
    for ax in axes:
        axes[ax].clear()

    c_rate=count_rate.getData()[0]
    axes["top"].plot(counter.getIndex()*1E-12, counter.getData()[0])
    axes["top"].text(0.01, 0.9, f"Countrate : {c_rate:.2e}",  transform=axes["top"].transAxes, fontsize=9)
    axes["top"].set_title("DLS Time Trace")
    axes["top"].set_xlabel("time (s)")
    axes["top"].set_ylabel("Counts")

    g2_cpp= log_histo.getDataNormalizedG2()
    g2_py= log_histo2.getDataNormalizedG2()
    axes["bottom"].semilogx(log_histo.getBinEdges()[1:] * 1e-12, g2_cpp, label="C++")
    axes["bottom"].semilogx(log_histo2.getBinEdges()[1:] * 1e-12, g2_py, label="Python")
    axes["bottom"].semilogx(x_range, y_range, alpha=0.5, label='Theory')
    axes["bottom"].set_title("Signal Autocorrelation")
    axes["bottom"].legend(loc='upper right')
    axes["bottom"].set_xlabel(r"$\tau$ (s)")
    axes["bottom"].set_ylabel(r"$g_2(\tau$)")

    fig.canvas.draw()