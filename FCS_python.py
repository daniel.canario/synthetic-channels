import TimeTagger as TT
import numpy as np
import matplotlib.pyplot as plt

#------------------------------------------------------------------------------
#============================= Time Tagger Setup ==============================
#------------------------------------------------------------------------------

virtual_tagger = TT.createTimeTaggerVirtual()
virtual_tagger.setReplaySpeed(1)

#------------------------------------------------------------------------------
#============================= Numeric Constants ==============================
#------------------------------------------------------------------------------

rng = np.random.default_rng(seed=1,)
OUT_CHANNEL=1
CORR_TIME=1E10 # 1 microsecond in picoseconds
T_period=1E6
std_dev=np.sqrt(T_period/CORR_TIME/2) # from theory of Brownian motion
COUNTRATE=1E6 # in Hz
N_FOCUS=3
BOUNDARY=8


#------------------------------------------------------------------------------
#================================= FCS Class ==================================
#------------------------------------------------------------------------------


class FCS(TT.Experimental.PhotonGenerator):
    def __init__(self, tagger, countrate, channel):
        super().__init__(tagger, countrate, channel, 1)
        self.N=N_FOCUS*BOUNDARY**2
        r=np.sqrt(np.random.uniform(0, BOUNDARY**2, self.N))
        self.positions = (r*np.cos(0), r*np.sin(0))
        self.finalize_init()

    def __del__(self):
        self.stop()

    def get_intensity(self):        
        self.positions += rng.normal(0, std_dev, [2, self.N])
        self.positions=self.positions.clip(-np.sqrt(np.pi)/2*BOUNDARY, np.sqrt(np.pi)/2*BOUNDARY)
        radius_sq=self.positions[0]**2+self.positions[1]**2
        return np.sum(np.exp(-2 * radius_sq))*2/N_FOCUS

fcs_signal_py = FCS(tagger=virtual_tagger,
                 countrate=COUNTRATE,
                 channel=OUT_CHANNEL)

fcs_signal_cpp=TT.Experimental.FcsSignalGenerator(
                    tagger=virtual_tagger,
                    correlation_time=CORR_TIME,
                    countrate=COUNTRATE,
                    N_focus=N_FOCUS,
                    output_channel=OUT_CHANNEL + 1)

#------------------------------------------------------------------------------
#============================ Measurement Objects =============================
#------------------------------------------------------------------------------

E_START=-4
E_END=0
OUT_CHANNEL = 1
BIN_WIDTH = 1e8
N_BINS = int(1e3)

log_histo = TT.HistogramLogBins(
    tagger=virtual_tagger,
    click_channel=OUT_CHANNEL,
    start_channel=OUT_CHANNEL,
    exp_start=E_START,
    exp_stop=E_END,
    n_bins=100,
)

log_histo2 = TT.HistogramLogBins(
    tagger=virtual_tagger,
    click_channel=OUT_CHANNEL + 1,
    start_channel=OUT_CHANNEL +1,
    exp_start=E_START,
    exp_stop=E_END,
    n_bins=100,
)

counter = TT.Counter(
    tagger=virtual_tagger,
    channels=[OUT_CHANNEL],
    binwidth=BIN_WIDTH,
    n_values=N_BINS,
)

count_rate=TT.Countrate(
    tagger=virtual_tagger,
    channels=[OUT_CHANNEL])

#------------------------------------------------------------------------------
#=========================== Plot Synthetic Signal ============================
#------------------------------------------------------------------------------

PHI = 0.5 * (1 + np.sqrt(5))  # golden ratio

fig, axes = plt.subplot_mosaic(
    [["top"],
     ["bottom"]], width_ratios=[1 / PHI]
)
fig.subplots_adjust(hspace=.5)
fig.canvas.manager.set_window_title('FCS Synthetic Channel')

tau=np.logspace(E_START,E_END,50)
tau_D=CORR_TIME*1E-12
g0=1/N_FOCUS #if not sticky else 4*boundary**2/(np.pi)
g2_theo=g0*1/(1+tau/tau_D) +1

while plt.fignum_exists(1):
    plt.pause(.25)
    for ax in axes:
        axes[ax].clear()

    c_rate=count_rate.getData()[0]
    axes["top"].plot(counter.getIndex()*1E-12, counter.getData()[0])
    axes["top"].text(0.01, 0.9, f"Countrate : {c_rate:.2e}",  transform=axes["top"].transAxes, fontsize=9)
    axes["top"].set_title("FCS Time Trace")
    axes["top"].set_xlabel("time (s)")
    axes["top"].set_ylabel("Counts")

    g2_cpp= log_histo.getDataNormalizedG2()
    g2_py= log_histo2.getDataNormalizedG2()
    axes["bottom"].semilogx(log_histo.getBinEdges()[1:] * 1e-12, g2_cpp, label="C++")
    axes["bottom"].semilogx(log_histo2.getBinEdges()[1:] * 1e-12, g2_py, label="Python")
    axes["bottom"].semilogx(tau, g2_theo, alpha=0.5, label='Theory')
    axes["bottom"].set_title("Signal Autocorrelation")
    axes["bottom"].legend(loc='upper right')
    axes["bottom"].set_xlabel(r"$\tau$ (s)")
    axes["bottom"].set_ylabel(r"$g_2(\tau$)")

    fig.canvas.draw()